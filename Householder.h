

/** @file Householder.h
 *
 * @brief implement the various types of Householder transformations
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2016-06-17
 */

#ifndef HOUSEHOLDER_H
#define HOUSEHOLDER_H

#include <array>
#include <limits>
#include <type_traits>

/// namespace for routines doing Householder transformations
namespace HouseholderTransforms {
/** @brief apply a Householder transformation from the left
 *
 * Given a Householder vector H, calculates @f$\left(\mathbbm{1} - 2\frac{
 * HH^T}{H^TH}\right)M@f$.
 *
 * @param M      matrix to which to apply the transformation
 * @param H      Householder vector
 * @param precalcnorm
 *               precalculated norm of H (if available, optional)
 *
 * @tparam N     length of Householder vector
 * @tparam I     work on matrix subblock starting in row I
 * @tparam J     work on matrix subblock starting in column J
 * @tparam K     Householder vector stored in H[K]...H[K + N - 1]
 * @tparam NCOL  work on a submatrix of size NxNCOL
 * @tparam MTYPE type of matrix M
 * @tparam HTYPE type of Householder vector H
 * @tparam NTYPE type of precomputed norm of H
 *
 * @returns norm of Householder vector H
 *
 * @note This routine works in-place, so the contents of M are modified.
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2016-06-17
 */
template <unsigned N, unsigned I = 0, unsigned J = 0, unsigned K = 0,
         unsigned NCOL = N, typename MTYPE, typename HTYPE,
         typename NTYPE = decltype((*static_cast<MTYPE*>(nullptr))[0][0] *
                 (*static_cast<HTYPE*>(nullptr))[0])>
NTYPE applyHHVasQM(MTYPE& M, const HTYPE& H,
        NTYPE precalcnorm = std::numeric_limits<NTYPE>::quiet_NaN()) noexcept(
        noexcept(M[0][0] * H[0]))
{
    static_assert(N > 0,
            "Dimension of Householder vector N must be positive.");
    static_assert(NCOL > 0,
            "Number of columns NCOL in matrix must be positive.");
    typedef typename std::decay<decltype(M[0][0] * H[0])>::type value_type;
    static_assert(std::is_same<NTYPE, value_type>::value,
            "Wrong type passed for normalisation.");
    std::array<value_type, NCOL> p;
    value_type norm = precalcnorm;
    if (precalcnorm != precalcnorm) {
        norm = 0;
        for (unsigned i = 0; i < N; ++i) norm += H[K + i] * H[K + i];
    }
    if (norm > 0) {
        const auto norm2 = norm / 2;
        for (unsigned i = 0; i < NCOL; ++i) {
            value_type tmp = 0;
            for (unsigned j = 0; j < N; ++j)
                tmp += M[I + j][J + i] * H[K + j];
            p[i] = tmp / norm2;
        }
        for (unsigned i = 0; i < N; ++i) {
            for (unsigned j = 0; j < NCOL; ++j)
                M[I + i][J + j] -= H[K + i] * p[j];
        }
    }
    return norm;
}

/** @brief apply a Householder transformation from the right
 *
 * Given a Householder vector H, calculates @f$M\left(\mathbbm{1} - 2\frac{
 * HH^T}{H^TH}\right)@f$.
 *
 * @param M      matrix to which to apply the transformation
 * @param H      Householder vector
 * @param precalcnorm
 *               precalculated norm of H (if available, optional)
 *
 * @tparam N     length of Householder vector
 * @tparam I     work on matrix subblock starting in row I
 * @tparam J     work on matrix subblock starting in column J
 * @tparam K     Householder vector stored in H[K]...H[K + N - 1]
 * @tparam NROW  work on a submatrix of size NROWxN
 * @tparam MTYPE type of matrix M
 * @tparam HTYPE type of Householder vector H
 * @tparam NTYPE type of precomputed norm of H
 *
 * @returns norm of Householder vector H
 *
 * @note This routine works in-place, so the contents of M are modified.
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2016-06-17
 */
template <unsigned N, unsigned I = 0, unsigned J = 0, unsigned K = 0,
         unsigned NROW = N, typename MTYPE, typename HTYPE,
         typename NTYPE = decltype((*static_cast<MTYPE*>(nullptr))[0][0] *
                 (*static_cast<HTYPE*>(nullptr))[0])>
NTYPE applyHHVasMQT(MTYPE& M, const HTYPE& H,
        NTYPE precalcnorm = std::numeric_limits<NTYPE>::quiet_NaN()) noexcept(
        noexcept(M[0][0] * H[0]))
{
    static_assert(N > 0,
            "Dimension of Householder vector N must be positive.");
    static_assert(NROW > 0,
            "Number of rows NROW in matrix must be positive.");
    typedef typename std::decay<decltype(M[0][0] * H[0])>::type value_type;
    static_assert(std::is_same<NTYPE, value_type>::value,
            "Wrong type passed for normalisation.");
    std::array<value_type, NROW> p;
    value_type norm = precalcnorm;
    if (precalcnorm != precalcnorm) {
        norm = 0;
        for (unsigned i = 0; i < N; ++i) norm += H[K + i] * H[K + i];
    }
    if (norm > 0) {
        const auto norm2 = norm / 2;
        for (unsigned i = 0; i < NROW; ++i) {
            value_type tmp = 0;
            for (unsigned j = 0; j < N; ++j)
                tmp += M[I + i][J + j] * H[K + j];
            p[i] = tmp / norm2;
        }
        for (unsigned i = 0; i < NROW; ++i) {
            for (unsigned j = 0; j < N; ++j)
                M[I + i][J + j] -= p[i] * H[K + j];
        }
    }
    return norm;
}

/** @brief apply a Householder transformation from both both sides
 *
 * Given a Householder vector H, calculates @f$\left(\mathbbm{1} - 2\frac{
 * HH^T}{H^TH}\right)M\left(\mathbbm{1}-2\frac{HH^T}{H^TH}\right)@f$.
 *
 * @param M      matrix to which to apply the transformation
 * @param H      Householder vector
 * @param precalcnorm
 *               precalculated norm of H (if available, optional)
 *
 * @tparam N     length of Householder vector
 * @tparam I     work on matrix subblock starting in row I
 * @tparam J     work on matrix subblock starting in column J
 * @tparam K     Householder vector stored in H[K]...H[K + N - 1]
 * @tparam CALCLOWERHALF
 *               true if the lower left half of the matrix should
 *               be modified (including the diagonal)
 * @tparam CALCUPPERHALF
 *               true if the upper right half of the matrix should
 *               be modified (including the diagonal)
 * @tparam SYMMETRIC
 *               assume M to be symmetric, and calculate based on the
 *               lower half of the matrix (but set both halves)
 * @tparam MTYPE type of matrix M
 * @tparam HTYPE type of Householder vector H
 * @tparam NTYPE type of precomputed norm of H
 *
 * @returns norm of Householder vector H
 *
 * @note This routine works in-place, so the contents of M are modified.
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2016-06-17
 */
template <unsigned N, unsigned I = 0, unsigned J = 0, unsigned K = 0,
         bool CALCLOWERHALF = true, bool CALCUPPERHALF = true,
         bool SYMMETRIC = false, typename MTYPE, typename HTYPE,
         typename NTYPE = decltype((*static_cast<MTYPE*>(nullptr))[0][0] *
                 (*static_cast<HTYPE*>(nullptr))[0])>
NTYPE applyHHVasQMQT(MTYPE& M, const HTYPE& H,
        NTYPE precalcnorm = std::numeric_limits<NTYPE>::quiet_NaN()) noexcept(
        noexcept(M[0][0] * H[0]))
{
    static_assert(N > 0,
            "Dimension of Householder vector N must be positive.");
    static_assert(CALCLOWERHALF || CALCUPPERHALF,
            "Must calculater at least one of lower or upper half of matrix.");
    typedef typename std::decay<decltype(M[0][0] * H[0])>::type value_type;
    static_assert(std::is_same<NTYPE, value_type>::value,
            "Wrong type passed for normalisation.");
    std::array<value_type, N> p;
    value_type norm = precalcnorm;
    if (precalcnorm != precalcnorm) {
        norm = 0;
        for (unsigned i = 0; i < N; ++i) norm += H[K + i] * H[K + i];
    }
    if (norm > 0) {
        const auto norm2 = norm / 2;
        for (unsigned i = 0; i < N; ++i) {
            value_type tmp = 0;
            for (unsigned j = 0; j < N; ++j) tmp += M[I + i][J + j] * H[K + j];
            p[i] = tmp / norm2;
        }
        {
            value_type eta = 0;
            for (unsigned i = 0; i < N; ++i) eta += H[K + i] * p[i];
            eta /= norm;
            for (unsigned i = 0; i < N; ++i) p[i] -= eta * H[K + i];
        }
        for (unsigned i = 0; i < N; ++i) {
            if (CALCLOWERHALF && CALCUPPERHALF) {
                if (SYMMETRIC) {
                    for (unsigned j = 0; j < i; ++j) {
                        const auto tmp = M[I + i][J + j] -
                            H[K + i] * p[j] - p[i] * H[K + j];
                        M[I + i][J + j] = tmp;
                        M[I + j][J + i] = tmp;
                    }
                    M[I + i][J + i] -= 2 * H[K + i] * p[i];
                } else {
                    for (unsigned j = 0; j < N; ++j)
                        M[I + i][J + j] -= H[K + i] * p[j] + p[i] * H[K + j];
                }
            } else if (CALCLOWERHALF && !CALCUPPERHALF) {
                for (unsigned j = 0; j <= i; ++j)
                    M[I + i][J + j] -= H[K + i] * p[j] + p[i] * H[K + j];
            } else if (!CALCLOWERHALF && CALCUPPERHALF) {
                for (unsigned j = i; j < N; ++j)
                    M[I + i][J + j] -= H[K + i] * p[j] + p[i] * H[K + j];
            }
        }
    }
    return norm;
}

} // end of namespace HouseholderTransforms

#endif // HOUSEHOLDER_H

// vim: sw=4:tw=78:ft=cpp:et


