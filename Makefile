CXXFLAGS=-std=c++11 -O2 -march=native -pedantic -Wall -Wextra

TARGETS = matmup3

all: $(TARGETS)

clean:
	rm -f $(TARGETS)

distclean: clean
	rm -f *.o *~

matmup3: matmup3.cc dotprod.h Householder.h
	$(CXX) $(CXXFLAGS) -o $@ $<
