#ifndef CHOLDCMP
#define CHOLDCMP

#include <cstdio>
#include <vector>
#include <array>
#include <type_traits>
#include <cmath>
#include "dotprod.h"

template <unsigned N1, unsigned N2, unsigned it1r,
	 unsigned it1c, unsigned it2r, unsigned it2c> struct transpose_row{
	     template <typename I1TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, OTYPE& O) noexcept
		 {
		     O[it2r + N2 - 1][it2c + N1 - 1] = F[it1r + N1 - 1][it1c + N2 - 1];
		     transpose_row<N1 - 1, N2, it1r, it1c, it2r, it2c>::doIt(F, O);
		 }
	     template <typename I1TYPE>
		 static void doIt(I1TYPE& F) noexcept
		 { 
		     F[it1r + N2 - 1][it1c + N1 - 1] = F[it1r + N2 - 1][it1c + N1 - 1]\
						       + F[it1r +N1 - 1][it1c + N2 - 1];
		     F[it1r + N1 - 1][it1c + N2 - 1] = F[it1r + N2 - 1][it1c + N1 - 1]\
						       - F[it1r + N1 - 1][it1c + N2 - 1];
		     F[it1r + N2 - 1][it1c + N1 - 1] = F[it1r + N2 - 1][it1c + N1 - 1]\
						       - F[it1r + N1 - 1][it1c + N2 - 1];
		     transpose_row<N1 - 1, N2, it1r, it1c, it2r, it2c>::doIt(F);
		 }
	 };
template <unsigned N2, unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c> struct transpose_row<1u, N2, it1r, it1c, it2r, it2c>{
	     template <typename I1TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, OTYPE& O) noexcept
		 {
		     O[it2r + N2 - 1][it2c] = F[it1r][it1c + N2 - 1];
		 }
	     template <typename I1TYPE>
		 static void doIt(I1TYPE& F) noexcept
		 {
		     F[it1r + N2 - 1][it1c] = F[it1r + N2 - 1][it1c] +\
						   F[it1r][it1c + N2 - 1];
		     F[it1r][it1c + N2 - 1] = F[it1r + N2 - 1][it1c] -\
						   F[it1r][it1c + N2 - 1];
		     F[it1r + N2 - 1][it1c] = F[it1r + N2 - 1][it1c] -\
						   F[it1r][it1c + N2 - 1];
		 }
	 };

template <unsigned N1, unsigned N2, unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c> struct transpose_col{
	     template <typename I1TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, OTYPE& O) noexcept
		 {
		     O[it2r + N2 - 1][it2c + N1 - 1] = F[it1r + N1 - 1][it1c + N2 - 1];
		     transpose_col<N1, N2 - 1, it1r, it1c, it2r, it2c>::doIt(F, O);
		 }
	 };
template <unsigned N1, unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c> struct transpose_col<N1, 1u, it1r, it1c, it2r, it2c>{
	     template <typename I1TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, OTYPE& O) noexcept
		 {
		     O[it2r][it2c + N1 - 1] = F[it1r + N1 - 1][it1c];
		 }
	 };

template <unsigned N1, unsigned N2, unsigned it1r = 0u, unsigned it1c = 0u,
	 unsigned it2r = 0u, unsigned it2c = 0u> struct transpose{
	     template <typename I1TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, OTYPE& O) noexcept
		 { 
		     O[it2r + N2 - 1][it2c + N1 - 1] = F[it1r + N1 - 1][it1c + N2 - 1];
		     transpose_row<N1 - 1, N2, it1r, it1c, it2r, it2c>::doIt(F, O);
		     transpose_col<N1, N2 - 1, it1r, it1c, it2r, it2c>::doIt(F, O);
		     transpose<N1 - 1, N2 - 1, it1r, it1c, it2r, it2c>::doIt(F, O);
		 }
	     template <typename I1TYPE>
		 static void doIt(I1TYPE& F) noexcept
		 { 
		     transpose_row<N1 - 1, N2, it1r, it1c, it2r, it2c>::doIt(F);
		     transpose<N1 - 1, N2 - 1, it1r, it1c, it2r, it2c>::doIt(F);
		 }
	 };
template <unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c> struct transpose<1u,1u, it1r, it1c, it2r, it2c>{
	     template <typename I1TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, OTYPE& O) noexcept
		 { 
		     O[it2r][it2c] = F[it1r][it1c];
		 }
	     template <typename I1TYPE>	
		 static void doIt(I1TYPE& F) noexcept
		 { 
		     F[it1c][it1r] = F[it1r][it1c];
		 }
	 };
template <unsigned N2, unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c> struct transpose<1u, N2, it1r, it1c, it2r, it2c>{
	     template <typename I1TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, OTYPE& O) noexcept
		 {
		     O[it2r + N2 - 1][it2c] = F[it1r][it1c + N2 - 1];
		     transpose_col<1, N2 - 1, it1r, it1c, it2r, it2c>::doIt(F, O);
		 }

	 };
template <unsigned N1, unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c> struct transpose<N1, 1u, it1r, it1c, it2r, it2c>{
	     template <typename I1TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, OTYPE& O) noexcept
		 {
		     O[it2r][it2c + N1 - 1] = F[it1r + N1 - 1][it1c];
		     transpose_row<N1 - 1, 1, it1r, it1c, it2r, it2c>::doIt(F, O);
		 }

	 };

// structure template to calculate the matrix product of two matrices
//
//
//
template <unsigned N1, unsigned N2, unsigned N3, unsigned N4,
	 bool negate, unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c, unsigned it3r, unsigned it3c> struct _matmulp_row{
	     template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		 { 
		     O[it3r + N1 - 1][it3c + N4 - 1] =\
						      dotprod<N2 + it1c>(F[it1r + N1 - 1],G[it2c + N4 - 1]) -\
						      dotprod<it1c>(F[it1r + N1 -1], G[it2c + N4 - 1]); 
		     _matmulp_row<N1 - 1, N2, N3, N4, negate, it1r, it1c, it2r, it2c, it3r,\
			 it3c>::doIt(F, G, O);
		 }
	 };
template <unsigned N2, unsigned N3, unsigned N4, bool negate,
          unsigned it1r, unsigned it1c, unsigned it2r, unsigned it2c,
          unsigned it3r, unsigned it3c>\
	      struct _matmulp_row<1u, N2, N3, N4, negate, it1r, it1c, it2r, it2c, it3r, it3c>{
		  template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		      static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		      { 
			  O[it3r][it3c + N4 - 1] =\
						  dotprod<N2 + it1c>(F[it1r], G[it2c + N4 - 1]) -\
						  dotprod<it1c>(F[it1r], G[it2c + N4 - 1]); 
		      }
	      };
template <unsigned N1, unsigned N2, unsigned N3, unsigned N4,
	 unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c, unsigned it3r, unsigned it3c> 
	 struct _matmulp_row<N1, N2, N3, N4, true, it1r, it1c, it2r, it2c, it3r, it3c>{
	     template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		 { 
		     O[it3r + N1 - 1][it3c + N4 - 1] =\
						      dotprod<it1c>(F[it1r + N1 -1], G[it2c + N4 - 1]) -\
						      dotprod<N2 + it1c>(F[it1r + N1 - 1],G[it2c + N4 - 1]);
		     _matmulp_row<N1 - 1, N2, N3, N4, true, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
		 }
	 };
template <unsigned N2, unsigned N3, unsigned N4,
	 unsigned it1r, unsigned it1c, unsigned it2r, unsigned it2c,
	 unsigned it3r, unsigned it3c>
	 struct _matmulp_row<1u, N2, N3, N4, true, it1r, it1c, it2r, it2c, it3r, it3c>{
	     template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		 { 
		     O[it3r][it3c + N4 - 1] =\
					     dotprod<it1c>(F[it1r], G[it2c + N4 - 1]) -\
					     dotprod<N2 + it1c>(F[it1r], G[it2c + N4 - 1]);
		 }
	 };

template <unsigned N1, unsigned N2, unsigned N3, unsigned N4,
	 bool negate, unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c, unsigned it3r, unsigned it3c> struct _matmulp_col{
	     template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		 {
		     O[it3r + N1 - 1][it3c + N4 - 1] =\
						      dotprod<N2 + it1c>(F[it1r + N1 - 1], G[it2c + N4 - 1]) -\
						      dotprod<it1c>(F[it1r + N1 - 1], G[it2c + N4 - 1]);
		     _matmulp_col<N1, N2, N3, N4 - 1, negate, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
		 }
	 };
template <unsigned N1, unsigned N2, unsigned N3, bool negate,
	 unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c, unsigned it3r, unsigned it3c>\
	     struct _matmulp_col<N1, N2, N3, 1u, negate, it1r, it1c, it2r, it2c,\
	     it3r, it3c>{
		 template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		     static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		     { 
			 O[it3r + N1 - 1][it3c] = \
						  dotprod<N2 + it1c>(F[it1r + N1 - 1], G[it2c]) -\
						  dotprod<it1c>(F[it1r + N1 - 1], G[it2c]);
		     }
	     };
template <unsigned N1, unsigned N2, unsigned N3, unsigned N4,
	 unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c, unsigned it3r, unsigned it3c>\
	     struct _matmulp_col<N1, N2, N3, N4, true, it1r, it1c, it2r, it2c, it3r, it3c>{
		 template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		     static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		     {
			 O[N1 + it3r - 1][N4 + it3c - 1] =\
							  dotprod<it1c>(F[it1r + N1 - 1], G[it2c + N4 - 1]) -\
							  dotprod<N2 + it1c>(F[it1r + N1 - 1], G[it2c + N4 - 1]);
			 _matmulp_col<N1, N2, N3, N4 - 1, true, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
    }
	     };
template <unsigned N1, unsigned N2, unsigned N3,
	 unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c, unsigned it3r, unsigned it3c>\
	     struct _matmulp_col<N1, N2, N3, 1u, true, it1r, it1c, it2r, it2c,\
	     it3r, it3c>{
		 template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		     static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		     { 
			 O[it3r + N1 - 1][it3c] = \
						  dotprod<it1c>(F[it1r + N1 - 1], G[it2c]) -\
						  dotprod<N2 + it1c>(F[it1r + N1 - 1], G[it2c]);
		     }
	     };

template <unsigned N1, unsigned N2, unsigned N3, unsigned N4,
	 bool negate, unsigned it1r, unsigned it1c,
	 unsigned it2r, unsigned it2c, unsigned it3r,
	 unsigned it3c> struct _matmulp{
	     template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		 { 
		     O[it3r + N1 - 1][it3c + N4 - 1] = \
						       dotprod<N2 + it1c>(F[it1r + N1 - 1], G[it2c + N4 - 1]) -\
						       dotprod<it1c>(F[it1r + N1 - 1], G[it2c + N4 - 1]); 
		     _matmulp_row<N1 - 1, N2, N3, N4, negate, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
		     _matmulp_col<N1, N2, N3, N4 - 1, negate, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
		     _matmulp<N1 - 1, N2, N3, N4 - 1, negate, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
		 }
	 };
template <unsigned N2, unsigned N3, bool negate, unsigned it1r,
	 unsigned it1c, unsigned it2r, unsigned it2c,
	 unsigned it3r, unsigned it3c>\
	     struct _matmulp<1u, N2, N3, 1u, negate, it1r, it1c, it2r, it2c,it3r, it3c>{
		 template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		     static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		     { 
			 O[it3r][it3c] = dotprod<N2 + it1c>(F[it1r], G[it2c]) -\
					 dotprod<it1c>(F[it1r], G[it2c]); 
		     }
	     };
template <unsigned N1, unsigned N2, unsigned N3, bool negate, unsigned it1r,
	 unsigned it1c, unsigned it2r, unsigned it2c,
	 unsigned it3r, unsigned it3c>\
	     struct _matmulp<N1, N2, N3, 1u, negate, it1r, it1c, it2r, it2c,it3r, it3c>{
		 template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		     static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		     { 
			 O[it3r + N1 - 1][it3c] = dotprod<N2 + it1c>(F[it1r + N1 - 1], G[it2c]) -\
					 dotprod<it1c>(F[it1r + N1 - 1], G[it2c]);
			 _matmulp_row<N1 - 1, N2, N3, 1u, negate, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
		     }
	     };
template <unsigned N2, unsigned N3, unsigned N4, bool negate, unsigned it1r,
	 unsigned it1c, unsigned it2r, unsigned it2c,
	 unsigned it3r, unsigned it3c>\
	     struct _matmulp<1u, N2, N3, N4, negate, it1r, it1c, it2r, it2c,it3r, it3c>{
		 template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		     static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		     { 
			 O[it3r][it3c + N4 - 1] = dotprod<N2 + it1c>(F[it1r], G[it2c + N4 - 1]) -\
					 dotprod<it1c>(F[it1r], G[it2c + N4 - 1]);
			 _matmulp_col<1u, N2, N3, N4 - 1, negate, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
		     }
	     };
template <unsigned N1, unsigned N2, unsigned N3, unsigned N4, 
	 unsigned it1r, unsigned it1c, unsigned it2r, unsigned it2c, 
	 unsigned it3r, unsigned it3c>\
	     struct _matmulp<N1, N2, N3, N4, true, it1r, it1c, it2r, it2c, it3r, it3c>{
		 template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		     static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		     { 
			 O[it3r + N1 - 1][it3c + N4 - 1] = \
							   dotprod<it1c>(F[it1r + N1 - 1],G[it2c + N4 - 1]) -\
							   dotprod<N2 + it1c>(F[it1r + N1 - 1], G[it2c + N4 - 1]);
			 _matmulp_row<N1 - 1, N2, N3, N4, true, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
			 _matmulp_col<N1, N2, N3, N4 - 1, true, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
			 _matmulp<N1 - 1, N2, N3, N4 - 1, true, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
		     }
	     };
template <unsigned N1, unsigned N2, unsigned N3, unsigned it1r, unsigned it1c,
	 unsigned it2r, unsigned it2c, unsigned it3r, unsigned it3c>
	 struct _matmulp<N1, N2, N3, 1u, true, it1r, it1c, it2r, it2c, it3r, it3c>{
	     template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		 {
		     O[it3r + N1 - 1][it3c] = dotprod<it1c>(F[it1r + N1 - 1], G[it2c]) -\
				     dotprod<N2 + it1c>(F[it1r + N1 - 1], G[it2c]);
		     _matmulp_row<N1 - 1, N2, N3, 1, true, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
		 }
	 };
template <unsigned N2, unsigned N3, unsigned N4, unsigned it1r, unsigned it1c,
	 unsigned it2r, unsigned it2c, unsigned it3r, unsigned it3c>
	 struct _matmulp<1u, N2, N3, N4, true, it1r, it1c, it2r, it2c, it3r, it3c>{
	     template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		 {
		     O[it3r][it3c + N4 - 1] = dotprod<it1c>(F[it1r], G[it2c + N4 - 1]) -\
				     dotprod<N2 + it1c>(F[it1r], G[it2c + N4 - 1]);
		     _matmulp_col< 1u, N2, N3, N4 - 1, true, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);
		 }
	 };
template <unsigned N2, unsigned N3, unsigned it1r, unsigned it1c,
	 unsigned it2r, unsigned it2c, unsigned it3r, unsigned it3c>
	 struct _matmulp< 1u, N2, N3, 1u, true, it1r, it1c, it2r, it2c, it3r, it3c>{
	     template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		 {
		     O[it3r][it3c] = dotprod<it1c>(F[it1r], G[it2c]) -\
				     dotprod<N2 + it1c>(F[it1r], G[it2c]);
		 }
	 };

template <unsigned N1, unsigned N2, unsigned N3, unsigned N4,
	 bool negate = false,
	 unsigned it1r = 0u, unsigned it1c = 0u,
	 unsigned it2r = 0u, unsigned it2c = 0u,
	 unsigned it3r = 0u, unsigned it3c = 0u> struct matmulp{
	     template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		 {
		     static_assert(N2 == N3,"dimension mismatch for matrix multiplication"); 
		     constexpr unsigned M1 = sizeof(G)/sizeof(G[0]);
		     constexpr unsigned M2 = sizeof(G[0])/sizeof(G[0][0]);
		     typedef typename std::decay<decltype(G[0][0])>::type value_type;
		     std::array<std::array<value_type, M2>, M1> tranVal;
		     transpose<M1, M2>::doIt(G, tranVal);
		     _matmulp<N1, N2, N3, N4, negate, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, tranVal, O);
		 }
	 };
template <unsigned N1, unsigned N2, unsigned N3, unsigned N4,
	 bool negate = false,
	 unsigned it1r = 0u, unsigned it1c = 0u,
	 unsigned it2r = 0u, unsigned it2c = 0u,
	 unsigned it3r = 0u, unsigned it3c = 0u> struct matmulp_transp{
	     template <typename I1TYPE, typename I2TYPE, typename OTYPE>
		 static void doIt(I1TYPE& F, I2TYPE& G, OTYPE& O) noexcept
		 {
		     static_assert(N2 == N3,"dimension mismatch for matrix multiplication");
		     _matmulp<N1, N2, N3, N4, negate, it1r, it1c, it2r, it2c, it3r, it3c>::doIt(F, G, O);

		 }
	 };

// template structure to invert a triangular matrix
//
//
//
template <unsigned N1, unsigned it1r = 0, unsigned it1c = 0,
	 unsigned it2r = 0, unsigned it2c = 0> struct _invtrimat{
	     template <typename LTYPE, typename OTYPE>
		 static void doIt(LTYPE& L, OTYPE& OP) noexcept
		 {
		     constexpr unsigned N1a = (N1)/2;
		     constexpr unsigned N2a = N1 - N1a;
		     _invtrimat<N1a, it1r, it1c, it2r, it2c>::doIt(L, OP);
		     _invtrimat<N2a, it1r + N1a, it1c + N1a, it2r + N1a, it2c + N1a>::doIt(L, OP);
		     matmulp<N2a, N1a, N1a, N1a, true, N1a + it1r, it1c, it1r, it1c, N1a + it2r, it2c>::doIt(L, OP, OP);
		     matmulp<N2a, N2a, N2a, N1a, false, N1a + it2r, N1a + it2c, N1a + it2r, it2c, N1a + it2r, it2c>::doIt(OP, OP, OP);
		 }
	 };
template <unsigned it1r, unsigned it1c, unsigned it2r, unsigned it2c>\
	     struct _invtrimat<2u, it1r, it1c, it2r, it2c>{
		 template <typename LTYPE, typename OTYPE>
		     static void doIt(LTYPE& L, OTYPE& OP) noexcept
		     {
			 OP[it2r][it2c] = 1/(L[it1r][it1c]);
			 OP[it2r][1 + it2c] = 0.f;
			 OP[1 + it2r][1 + it2c] = 1/(L[1 + it1r][1 + it1c]);
			 OP[1 + it2r][it2c] = -1*L[1 + it1r][it1c]*OP[it2r][it2c]*OP[1 + it2r][1 + it2c];
		     }
	     };

template <unsigned it1r, unsigned it1c, unsigned it2r, unsigned it2c>\
	     struct _invtrimat<1u, it1r, it1c, it2r, it2c>{
		 template <typename LTYPE, typename OTYPE>
		     static void doIt(LTYPE& L, OTYPE& OP) noexcept
		     {
			 OP[it2r][it2c] = 1/(L[it1r][it1c]);
		     }
	     };
//structure template to set the upper triangle matrix to zero
//
//
//
//
template <unsigned N1, unsigned N2, unsigned it1r, unsigned it1c\
	     > struct setzero_row{
    template <typename LTYPE>
	static void doIt(LTYPE& L) noexcept
	{
	    L[it1r + N1 - 1][it1c + N2 - 2] = 0;
	    setzero_row<N1, N2 - 1, it1r, it1c>::doIt(L);
	}
};
template <unsigned N1, unsigned it1r, unsigned it1c\
	     > struct setzero_row<N1, 1u, it1r, it1c>{
    template <typename LTYPE>
	static void doIt(LTYPE& L) noexcept
	{ 
	    L[it1r + N1 - 1][it1c] = 0;
	}
};
template <unsigned N1, unsigned N2, unsigned it1r, unsigned it1c\
	     > struct setzero_column{
    template <typename LTYPE>
	static void doIt(LTYPE& L) noexcept
	{
	    L[it1r + N1 - 2][it1c + N2 - 1] = 0;
	    setzero_column<N1 - 1, N2, it1r, it1c>::doIt(L);
	}
};
template <unsigned N2, unsigned it1r, unsigned it1c\
	     > struct setzero_column<1u, N2, it1r, it1c>{
    template <typename LTYPE>
	static void doIt(LTYPE& L) noexcept
	{ 
	    L[it1r + 0][it1c + N2 - 1] = 0;
	}
};

template <unsigned N1, unsigned N2, bool fullmat = true, unsigned it1r = 0u,
	 unsigned it1c = 0u> struct setzero_mat{
    template <typename LTYPE>
	static void doIt(LTYPE& L) noexcept
	{
	    L[it1r + N1 - 1][it1c + N2 - 1] = 0;
	    setzero_column<N1, N2, it1r, it1c>::doIt(L);
	    setzero_row<N1, N2, it1r, it1c>::doIt(L);
	    setzero_mat<N1 - 1, N2 - 1, fullmat, it1r, it1c>::doIt(L);
	}
};
template <unsigned N1, unsigned N2, unsigned it1r,
	 unsigned it1c> struct setzero_mat<N1, N2, false, it1r, it1c>{
    template <typename LTYPE>
	static void doIt(LTYPE& L) noexcept
	{
	    setzero_column<N1, N2, it1r, it1c>::doIt(L);
	    setzero_mat<N1 - 1, N2 - 1, false, it1r, it1c>::doIt(L);
	}
};
template <unsigned it1r, unsigned it1c>\
	     struct setzero_mat<2u, 2u, false, it1r, it1c>{
    template <typename LTYPE>
	static void doIt(LTYPE& L) noexcept
	{
	    L[it1r][it1c + 1] = 0;
	}
};
template < unsigned it1r, unsigned it1c>\
	     struct setzero_mat<1u, 1u, true, it1r, it1c>{
    template <typename LTYPE>
	static void doIt(LTYPE& L) noexcept
	{
	    L[it1r][it1c] = 0;
	}
};

// structure template to add two matrices
//
//
//
template <unsigned N1, unsigned N2, unsigned it1r, unsigned it1c,
	 unsigned it2r, unsigned it2c> struct mat_add_row{
	     template <typename M1TYPE, typename M2TYPE>
		 static void doIt(M1TYPE& M1, M2TYPE& M2) noexcept
		 {
		     M2[it2r + N1 - 1][it2c + N2 - 1] += M1[it1r + N1 - 1][it1c + N2 - 1];
		     mat_add_row<N1 - 1, N2, it1r, it1c, it2r, it2c>::doIt(M1, M2);
		 }
	 };
template <unsigned N2,unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c> struct mat_add_row<1u, N2, it1r, it1c, it2r, it2c>{
	     template <typename M1TYPE, typename M2TYPE>
		 static void doIt(M1TYPE& M1, M2TYPE& M2) noexcept
		 {
		     M2[it2r][it2c + N2 - 1] += M1[it1r][it1c + N2 - 1];
		 }
	 };

template <unsigned N1, unsigned N2, unsigned it1r, unsigned it1c,
	 unsigned it2r, unsigned it2c> struct mat_add_column{
	     template <typename M1TYPE, typename M2TYPE>
		 static void doIt(M1TYPE& M1, M2TYPE& M2) noexcept
		 { 
		     M2[it2r + N1 - 1][it2c + N2 - 1] += M1[it1r + N1 - 1][it1c + N2 - 1];
		     mat_add_column<N1, N2 - 1, it1r, it1c, it2r, it2c>::doIt(M1, M2);
		 }
	 };
template <unsigned N1, unsigned it1r, unsigned it1c,unsigned it2r,
          unsigned it2c> struct mat_add_column<N1, 1u, it1r, it1c, it2r, it2c>{
	      template <typename M1TYPE, typename M2TYPE>
		  static void doIt(M1TYPE& M1, M2TYPE& M2) noexcept
		  { 
		      M2[it2r + N1 - 1][it2c] += M1[it1r + N1 - 1][it1c];
		  }
	  };

template <unsigned N1, unsigned N2, unsigned it1r = 0u,
	 unsigned it1c = 0u, unsigned it2r = 0u,
	 unsigned it2c = 0u> struct mat_add{
	     template <typename M1TYPE, typename M2TYPE>
		 static void doIt(M1TYPE& M1, M2TYPE& M2) noexcept
		 { 
		     M2[it2r + N1 - 1][it2c + N2 - 1] += M1[it1r + N1 - 1][it1c + N2 - 1];
		     mat_add_column<N1, N2 - 1, it1r, it1c, it2r, it2c>::doIt(M1, M2);
		     mat_add_row<N1 - 1, N2, it1r, it1c, it2r, it2c>::doIt(M1, M2);
		     mat_add<N1 - 1, N2 - 1, it1r, it1c, it2r, it2c>::doIt(M1, M2);
		 }
	 };
template <unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c> struct mat_add<1u, 1u, it1r, it1c, it2r, it2c>{
	     template <typename M1TYPE, typename M2TYPE>
		 static void doIt(M1TYPE& M1, M2TYPE& M2) noexcept
		 { 
		     M2[it2r][it2c] += M1[it1r][it1c];
		 }
	 };


// structure template to calculate the Cholesky decomposition
//
//
//
template <unsigned N1, unsigned it1r = 0u, unsigned it1c = 0u,
	 unsigned it2r = 0u, unsigned it2c = 0u> struct _choldcmp{
	     template <typename MTYPE, typename LTYPE>
		 static bool doIt(MTYPE& M, LTYPE& L) noexcept
		 { 
		     typedef typename std::decay<decltype(L[0][0])>::type val_type;
		     constexpr unsigned N1a = (N1)/2;
		     constexpr unsigned N2a = N1 - N1a;
		     if(!(_choldcmp<N1a, 0, 0, it2r, it2c>::doIt(M, L))) return false;
		     std::array<val_type, N1a> tempRow_1;
		     std::array<val_type, N2a> tempRow_2;
		     std::array<std::array<val_type, N1a>, N1a> tempArr_1; 
		     std::array<std::array<val_type, N2a>, N2a> tempArr_2; 
		     tempRow_1.fill(val_type(0));
		     tempRow_2.fill(val_type(0));
		     tempArr_1.fill(tempRow_1);
		     tempArr_2.fill(tempRow_2);
		     _invtrimat<N1a, it2r, it2c, 0, 0>::doIt(L, tempArr_1);
		     matmulp<N1a, N1a, N1a, N2a, false, 0, 0, 0, N1a,\
			 it2r, it2c + N1a>::doIt(tempArr_1, M, L);
		     transpose<N1a, N2a, it2r, it2c + N1a, it2r + N1a, it2c>::doIt(L,L);
		     matmulp<N2a, N1a, N1a, N2a, true, it2r + N1a, it2c, it2r, it2c + N1a,\
			 0, 0>::doIt(L, L, tempArr_2);
		     mat_add<N2a, N2a, N1a, N1a, 0, 0>::doIt(M, tempArr_2);
		     setzero_mat<N1, N1, false, it2r, it2c>::doIt(L);
		     return _choldcmp<N2a, 0, 0, it2r + N1a, it2c + N1a>::doIt(tempArr_2, L);
		 }
	 };
template <unsigned it1r, unsigned it1c, unsigned it2r,
	 unsigned it2c> struct _choldcmp<1u, it1r, it1c, it2r, it2c>{
	     template <typename MTYPE, typename LTYPE>
		 static bool doIt(MTYPE& M, LTYPE& L) noexcept
		 { 
		     if(M[it1r][it1c] <= 0) return false;
		     L[it2r][it2c] = std::sqrt(M[it1r][it1c]);
		     return true;
		 }
	 };

template <unsigned N1, unsigned it1r = 0u, unsigned it1c = 0u,
	 unsigned it2r = 0u, unsigned it2c = 0u> struct choldcmp{
	     template <typename IMTYPE, typename OLTYPE>
		 static bool doIt(IMTYPE& M, OLTYPE& L) noexcept
		 { 
		    return _choldcmp<N1, it1r, it1c, it2r, it2c>::doIt(M, L);
		 }
	 };

#endif