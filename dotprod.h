/** @file _dotprod.cc
 *
 * @brief dot product, optimised for SIMD operations where feasible
 */
#ifndef DOTPROD_H
#define DOTPROD_H

#include <vector>
#include <array>
#include <type_traits>

template <typename F, unsigned SIMDWIDTH> struct _SIMDVec {
typedef F type __attribute__((vector_size(SIMDWIDTH * sizeof(F))));
};
template <typename F, unsigned SIMDWIDTH>
using SIMDVec = typename _SIMDVec<F, SIMDWIDTH>::type;
template <typename F, unsigned SIMDWIDTH> struct _AlignedSIMDVec {
typedef F type __attribute__((vector_size(SIMDWIDTH * sizeof(F)),aligned(SIMDWIDTH * sizeof(F))));
};
template <typename F, unsigned SIMDWIDTH>
using AlignedSIMDVec = typename _AlignedSIMDVec<F, SIMDWIDTH>::type;

// give some semblance of portability - gcc and clang work flawlessly, and it
// even on compiles on FreeBSD/UltraSparc (albeit unvectorised, since my
// UltraSparc IIIi CPU doesn't support vectorisation of float quantities)
#ifdef __INTEL_COMPILER
#error "Intel compiler not supported - too difficult to program SIMDised."
#endif // __INTEL_COMPILER
#if defined(__GNUC__)
#if defined(__clang__)
// special hacks required to support clang
#if __has_builtin(__builtin_shufflevector)
/// shuffle SIMD vectors using clang's __builtin_shufflevector
#define simd_shuffle_2(v, i0, i1) \
    __builtin_shufflevector(v, v, i0, i1)
#define simd_shuffle_4(v, i0, i1, i2, i3) \
    __builtin_shufflevector(v, v, i0, i1, i2, i3)
#define simd_shuffle_8(v, i0, i1, i2, i3, i4, i5, i6, i7) \
    __builtin_shufflevector(v, v, i0, i1, i2, i3, i4, i5, i6, i7)
/// shuffle SIMD vectors using clang's __builtin_shufflevector
#define simd_shuffle2_2(v, w, i0, i1) \
    __builtin_shufflevector(v, w, i0, i1)
#define simd_shuffle2_4(v, w, i0, i1, i2, i3) \
    __builtin_shufflevector(v, w, i0, i1, i2, i3)
#define simd_shuffle2_8(v, w, i0, i1, i2, i3, i4, i5, i6, i7) \
    __builtin_shufflevector(v, w, i0, i1, i2, i3, i4, i5, i6, i7)
#else // __has_builtin(__builtin_shufflevector)
/// shuffle SIMD vectors, rely on compiler to generate reasonable code
#define simd_shuffle_2(v, i0, i1) (SIMDVec<decltype((v)[0]), 2>{\
        (v)[i0 & 1], (v)[i1 & 1] })
#define simd_shuffle_4(v, i0, i1, i2, i3) (SIMDVec<decltype((v)[0]), 4>{\
        (v)[i0 & 3], (v)[i1 & 3], (v)[i2 & 3], (v)[i3 & 3] })
#define simd_shuffle_8(v, i0, i1, i2, i3, i4, i5, i6, i7) \
    (SIMDVec<decltype((v)[0]), 8>{\
        (v)[i0 & 7], (v)[i1 & 7], (v)[i2 & 7], (v)[i3 & 7], \
        (v)[i4 & 7], (v)[i5 & 7], (v)[i6 & 7], (v)[i7 & 7] })
/// shuffle SIMD vectors, rely on compiler to generate reasonable code
#define simd_shuffle2_2(v, w, i0, i1, i2, i3) (SIMDVec<decltype((v)[0]), 2>{ \
        (i0 < 2) ? (v)[i0 & 1] : (w)[i0 & 1], \
        (i1 < 2) ? (v)[i1 & 1] : (w)[i1 & 1] })
#define simd_shuffle2_4(v, w, i0, i1, i2, i3) (SIMDVec<decltype((v)[0]), 4>{ \
        (i0 < 4) ? (v)[i0 & 3] : (w)[i0 & 3], \
        (i1 < 4) ? (v)[i1 & 3] : (w)[i1 & 3], \
        (i2 < 4) ? (v)[i2 & 3] : (w)[i2 & 3], \
        (i3 < 4) ? (v)[i3 & 3] : (w)[i3 & 3] })
#define simd_shuffle2_8(v, w, i0, i1, i2, i3, i4, i5, i6, i7) \
    (SIMDVec<decltype((v)[0]), 8>{ \
        (i0 < 8) ? (v)[i0 & 7] : (w)[i0 & 7], \
        (i1 < 8) ? (v)[i1 & 7] : (w)[i1 & 7], \
        (i2 < 8) ? (v)[i2 & 7] : (w)[i2 & 7], \
        (i3 < 8) ? (v)[i3 & 7] : (w)[i3 & 7], \
        (i4 < 8) ? (v)[i4 & 7] : (w)[i4 & 7], \
        (i5 < 8) ? (v)[i5 & 7] : (w)[i5 & 7], \
        (i6 < 8) ? (v)[i6 & 7] : (w)[i6 & 7], \
        (i7 < 8) ? (v)[i7 & 7] : (w)[i7 & 7], })
#endif // __has_builtin(__builtin_shufflevector)
#else // __clang__
/// shuffle SIMD vectors, use gcc's __builtin_shuffle
#define simd_shuffle_2(v, i0, i1) \
    __builtin_shuffle(v, SIMDVec<int, 2>{ i0, i1 })
#define simd_shuffle_4(v, i0, i1, i2, i3) \
    __builtin_shuffle(v, SIMDVec<int, 4>{ i0, i1, i2, i3 })
#define simd_shuffle_8(v, i0, i1, i2, i3, i4, i5, i6, i7) \
    __builtin_shuffle(v, SIMDVec<int, 8>{ i0, i1, i2, i3, i4, i5, i6, i7 })
/// shuffle SIMD vectors, use gcc's __builtin_shuffle
#define simd_shuffle2_2(v, w, i0, i1) \
    __builtin_shuffle(v, w, SIMDVec<int, 2>{ i0, i1 })
#define simd_shuffle2_4(v, w, i0, i1, i2, i3) \
    __builtin_shuffle(v, w, SIMDVec<int, 4>{ i0, i1, i2, i3 })
#define simd_shuffle2_8(v, w, i0, i1, i2, i3, i4, i5, i6, i7) \
    __builtin_shuffle(v, w, SIMDVec<int, 8>{ i0, i1, i2, i3, i4, i5, i6, i7 })
#endif // __clang__
#endif // __GNUC__

template <unsigned N> struct _dotprod_scalar {
    template <typename F, typename G>
    static auto doIt(F u, G v) noexcept(noexcept(u[0] * v[0]))
                        -> decltype(u[0] * v[0])
    { return _dotprod_scalar<N - 1>::doIt(u, v) +
            u[N - 1] * v[N - 1];
    }
};
template <> struct _dotprod_scalar<1u> {
    template <typename F, typename G>
    static auto doIt(F u, G v) noexcept(noexcept(u[0] * v[0]))
                        -> decltype(u[0] * v[0])
    { return u[0] * v[0]; }
};
template <> struct _dotprod_scalar<0u>{
    template <typename F, typename G>
    static auto doIt(F u, G v) noexcept(noexcept(u[0] * v[0]))
                        -> decltype(u[0] * v[0])
     { return true ? 0 : (u[0] * v[0]); }
};

template <unsigned N> struct _dotprod_simd_aligned {
    template <typename F, unsigned SIMDWIDTH>
    static SIMDVec<F, SIMDWIDTH> doIt(const AlignedSIMDVec<F, SIMDWIDTH>* u,
            const AlignedSIMDVec<F, SIMDWIDTH>* v) noexcept(noexcept(u[0] * v[0]))
    { return _dotprod_simd_aligned<N - 1>::template doIt<F, SIMDWIDTH>(u, v) + u[N - 1] * v[N - 1];
    }
};
template <> struct _dotprod_simd_aligned<1u> {
    template <typename F, unsigned SIMDWIDTH>
    static SIMDVec<F, SIMDWIDTH> doIt(const AlignedSIMDVec<F, SIMDWIDTH>* u,
            const AlignedSIMDVec<F, SIMDWIDTH>* v) noexcept(noexcept(u[0] * v[0]))
    { return u[0] * v[0]; }
};
template <> struct _dotprod_simd_aligned<0u>;

template <unsigned SIMDWIDTH> struct hadd;
template <> struct hadd<2u> {
    template <typename F>
    static F doIt(SIMDVec<F, 2u> v) noexcept
    { v += simd_shuffle_2(v, 1, 0);
      return v[0];
    }
};
template <> struct hadd<4u> {
    template <typename F>
    static F doIt(SIMDVec<F, 4u> v) noexcept
    {   
        v += simd_shuffle_4(v, 2, 3, 0, 1);
        v += simd_shuffle_4(v, 1, 0, 3, 2);
        return v[0];
    }
};
template <> struct hadd<8u> {
    template <typename F>
    static F doIt(SIMDVec<F, 8u> v) noexcept
   { 
        v += simd_shuffle_8(v, 4, 5, 6, 7, 0, 1, 2, 3);
        v += simd_shuffle_8(v, 2, 3, 0, 1, 6, 7, 4, 5);
        v += simd_shuffle_8(v, 1, 0, 3, 2, 5, 4, 7, 6);
        return v[0];
    }
};

template <typename T, unsigned N = 0> struct iscontiguous;
template <typename T> struct iscontiguous<T*, 0>
{ enum { value = true }; };
template <typename T, unsigned N> struct iscontiguous<T[N], 0>
{ enum { value = true }; };
template <typename T, unsigned N> struct iscontiguous<std::array<T, N> >
{ enum { value = true }; };
template <typename T> struct iscontiguous<std::vector<T>, 0 >
{ enum { value = true }; };
template <typename T> struct iscontiguous<T, 0>
{ enum { value = false }; };

template <unsigned N, typename F, typename G, bool ISCONTIGUOUS =
    iscontiguous<typename std::decay<F>::type>::value &&
    iscontiguous<typename std::decay<G>::type>::value>
struct _dotprod {
    static auto doIt(F u, G v) noexcept(noexcept(u[0] * v[0]))
                        -> decltype(u[0] * v[0])
    { return _dotprod_scalar<N>::doIt(u, v); }
};
template <unsigned N, typename F>
struct _dotprod<N, F, F, true> {
    static auto doIt(F u, F v) noexcept(noexcept(u[0] * v[0]))
                        -> decltype(u[0] * v[0])
    {
        typedef decltype(u[0] * v[0]) value_type;
        if (N >= 8) {
            auto retVal = hadd<8>::template doIt<value_type>(
                    _dotprod_simd_aligned<(N >> 3) ? (N >> 3) : 1>::template doIt<value_type, 8>(
                        (const AlignedSIMDVec<value_type, 8>*)(&u[0]),
                        (const AlignedSIMDVec<value_type, 8>*)(&v[0])));
            if (N > 8) retVal += _dotprod<N & 7,
                const decltype(retVal)*, const decltype(retVal)*,
                      true>::doIt(&u[N & ~7], &v[N & ~7]);
            return retVal;
        } else if (N >= 4) {
            auto retVal = hadd<4>::template doIt<value_type>(
                    _dotprod_simd_aligned<(N >> 2) ? (N >> 2) : 1>::template doIt<value_type, 4>(
                        (const AlignedSIMDVec<value_type, 4>*)(&u[0]),
                        (const AlignedSIMDVec<value_type, 4>*)(&v[0])));
            if (N > 4) retVal += _dotprod<N & 3,
                const decltype(retVal)*, const decltype(retVal)*,
                      true>::doIt(&u[N & ~3], &v[N & ~3]);
            return retVal;
        } else if (N >= 2) {
            auto retVal = hadd<2>::template doIt<value_type>(
                    _dotprod_simd_aligned<(N >> 1) ? (N >> 1) : 1>::template doIt<value_type, 2>(
                        (const AlignedSIMDVec<value_type, 2>*)(&u[0]),
                        (const AlignedSIMDVec<value_type, 2>*)(&v[0])));
            if (N > 2) retVal += _dotprod<N & 1,
                const decltype(retVal)*, const decltype(retVal)*,
                      true>::doIt(&u[N & ~1], &v[N & ~1]);
            return retVal;
        } else {
            return _dotprod_scalar<N>::doIt(u, v);
        }
    }
};

template <unsigned N, typename F, typename G>
auto dotprod(const F& u, const G& v) noexcept(noexcept(u[0] * v[0]))
                                    -> decltype(u[0] * v[0])
//{ return _dotprod_scalar<N>::doIt(u, v); }
{
  return _dotprod<N, const F&, const G&>::doIt(u, v); }

#endif

// vim: sw=4:tw=78:ft=cpp:et
