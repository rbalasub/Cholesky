/** @file testLDL.cpp
 * 
 * @brief unit tests for Cholesky decomposition, solving and inversion routines
 *
 * @author Adam Davis 
 * @date 2016-04-01
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2016-04-29
 */
#include <vector>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <random>
#include <chrono>
#include "HyperSphericalCoordinates.h"
#include "choldcmp.h"
#include "CholeskyDecomp.h"

template <typename F>
void printvec(unsigned n, const F& v) noexcept
{
    std::printf("(");
    for (unsigned i = 0; i < n; ++i) std::printf("%c%+24.16e",
            (0 == i) ? ' ' : ',', v[i]);
    std::printf(" )\n");
}

template <typename M>
void printmat(unsigned m, unsigned n, const M& mat) noexcept
{ for (unsigned i = 0; i < m; ++i) printvec(n, mat[i]); }

/// Cholesky decomposition unit test helper
template <unsigned N>
class TestCholesky {
    private:
        std::mt19937 m_rngengine;
        bool     m_printAlways;        ///< always print toy data
        double   m_maxErrInULPs;       ///< max. error in ULP

        double   m_abs_diff[N][N];     ///< absolute difference of matrix
        double   m_rel_diff[N][N];     ///< relative difference of matrix
        double   m_gen_mat[N][N];      ///< generated matrix
        double   m_lower_mat[N][N];    ///< matrix after decomposition;
        double   m_final_mat[N][N];    ///< final matrix after everything.

        double m_time; ///< time per decomposition in ns
        unsigned m_ndecomp; ///< number of decompositions done so far

    public:
        TestCholesky(unsigned seed,
                bool printAlways = false,
                double maxErrInULPs  = 1 << (14 + N)) :
            m_rngengine(seed), m_printAlways(printAlways), m_maxErrInULPs(maxErrInULPs),
            m_time(0), m_ndecomp(0)
        { }

        double time() const noexcept { return m_time; }

        void doPrint()
        {
            std::printf("Generated Covariance Matrix \n");
            printmat(N,N,m_gen_mat);
            std::printf("Final lower triangular matrix \n");
            printmat(N,N,m_lower_mat);
            std::printf("final recombined matrix\n");
            printmat(N,N,m_final_mat);
            std::printf("relative difference matrix\n");
            printmat(N,N,m_rel_diff);
        }

        void generate()
        {
            double L[N][N], D[N];
            // mix various different worst case condition numbers, so we have
            // ill and well conditioned matrices to invert
            const double lnCond = -M_LN2 * std::uniform_real_distribution<double>(
                    0., 40.)(m_rngengine);
            // start by generating an eigenvalue spectrum
            for (unsigned i = 0; i < N; ++i) {
                do {
                    D[i] = std::uniform_real_distribution<double>(0.5, 1.)(m_rngengine) *
                        std::exp(std::uniform_real_distribution<double>(lnCond, 0.)(m_rngengine));
                } while (std::abs(D[i]) < std::exp(lnCond));
            }
            if (m_printAlways) {
                std::printf("In %s: D=\n", __func__);
                printvec(N, D);
            }
            // now go for a matrix L (with eigenvalues all 1)
            for (unsigned i = 0; i < N; ++i) {
                // build a row vector in spherical coordinates in L[0]
                if (0 == i) {
                    L[0][0] = 1;
                    if (1 != N) L[0][1] = 0;
                } else if (1 == i && N > 1) {
                    do {
                        L[0][1] = std::uniform_real_distribution<double>(-M_PI, M_PI)(m_rngengine);
                        L[0][0] = std::sin(L[0][i]);
                    } while (0 == L[0][0]);
                    L[0][0] = 1 / L[0][0];
                } else if (N > 1) {
                    do {
                        L[0][1] = std::uniform_real_distribution<double>(-M_PI, M_PI)(m_rngengine);
                        for (unsigned j = 2; j <= i; ++j)
                            L[0][j] = std::uniform_real_distribution<double>(0., M_PI)(m_rngengine);
                        L[0][0] = std::cos(L[0][i]);
                    } while (0 == L[0][0]);
                    L[0][0] = 1 / L[0][0];
                }
                for (unsigned j = (2 > i + 1) ? 2 : (i + 1); j < N; ++j)
                    L[0][j] = 0.5 * M_PI;
                // convert angles back to cartesian coordinates
                fromHyperSphericalCoordinates<N>(L[0], L[0]);
                // and enter as a column vector of L, properly aligned
                for (unsigned j = 0; j < i; ++j)
                    L[N - 1 - j][N - 1 - i] = L[0][j];
                L[N - 1 - i][N - 1 - i] = 1;
                for (unsigned j = 1; j < N - 1 - i; ++j) L[j][N - 1 - i] = 0;
            }
            for (unsigned i = 1; i < N; ++i) L[0][i] = 0;
            if (m_printAlways) {
                std::printf("In %s: L=\n", __func__);
                printmat(N, N, L);
            }

            // make the covariance matrix
            for (unsigned i = 0; i < N; ++i) {
                for (unsigned j = 0; j <= i; ++j) {
                    double maxk = (i < j) ? i : j;
                    double tmp = 0;
                    for (unsigned k = 0; k <= maxk; ++k)
                        tmp += L[i][k] * D[k] * L[j][k];
                    m_gen_mat[j][i] = m_gen_mat[i][j] = tmp;
                }
            }
            if (m_printAlways) {
                std::printf("In %s: covariance matrix:\n", __func__);
                printmat(N, N, m_gen_mat);
            }
        }

        bool decompose()
        {
            // make lower matrix better behaved
            for (unsigned i = 0; i < N; ++i)
                for (unsigned j = i; ++j < N; m_lower_mat[i][j] = 0);
            const auto t0 = std::chrono::high_resolution_clock::now();
            if(!(choldcmp<N>::doIt(m_gen_mat, m_lower_mat))) return false;
            const auto t1 = std::chrono::high_resolution_clock::now();
            const std::chrono::duration<double> dt = t1 - t0;
            m_time *= double(m_ndecomp) / double(1 + m_ndecomp);
            m_time += 1e9 * dt.count() / double(++m_ndecomp);
            return true;
        }

        bool decomposeROOT()
        {
            double m[N * (N + 1) / 2];
            for (unsigned i = 0, k = 0; i < N; ++i)
                for (unsigned j = 0; j <= i; ++j) m[k++] = m_gen_mat[i][j];
            const auto t0 = std::chrono::high_resolution_clock::now();
            ROOT::Math::CholeskyDecomp<double, N> decomp(&m[0]);
            if (!decomp) return false;
            const auto t1 = std::chrono::high_resolution_clock::now();
            const std::chrono::duration<double> dt = t1 - t0;
            m_time *= double(m_ndecomp) / double(1 + m_ndecomp);
            m_time += 1e9 * dt.count() / double(++m_ndecomp);
            // put back L
            decomp.getL(&m[0]);
            for (unsigned i = 0, k = 0; i < N; ++i)
                for (unsigned j = 0; j <= i; ++j)
                    m_lower_mat[i][j] = m_lower_mat[j][i] = m[k++];
            return true;
        }

        void recombine()
        {
            for (unsigned i = 0; i < N; ++i) {
                for (unsigned j = 0; j <= i; ++j) {
                    double maxk = (i < j) ? i : j;
                    double tmp = 0;
                    for (unsigned k = 0; k <= maxk; ++k) {
                        tmp += m_lower_mat[i][k] * m_lower_mat[j][k];
                    }
                    m_final_mat[j][i] = m_final_mat[i][j] = tmp;
                }
            }
        }

        double analyze()
        {
            double retVal = 0.;
            for(unsigned i=0; i<N;++i){
                for(unsigned j=0; j<N;++j){
                    m_abs_diff[i][j] = std::abs(m_final_mat[i][j] - m_gen_mat[i][j]);
                    m_rel_diff[i][j] = (std::abs(m_gen_mat[i][j]) > std::numeric_limits<double>::epsilon()) ?
                        std::abs(1 - m_final_mat[i][j] / m_gen_mat[i][j]) : m_abs_diff[i][j];
                    retVal = std::max(retVal,m_rel_diff[i][j]);
                }
            }
            return retVal;
        }

        // returns #failed, #total
        std::pair<unsigned, unsigned> run()
        {
            unsigned fail = 0, total = 0;
            generate();    //generate the covariance matrices
            ++total;
            if (!decompose()) {
                ++fail;
                std::printf("Singular matrix, skipping comparison\n");
            }
            if (!fail) recombine();

            //analyze the difference
            ++total;
            const auto maxreldiff = (!fail) ? analyze() :
                std::numeric_limits<double>::max();
            if (maxreldiff > m_maxErrInULPs *
                    std::numeric_limits<double>::epsilon()) {
                ++fail;
            }
            if (m_printAlways || fail) {
                doPrint();
                std::printf("Maximum absolute value of relative difference: "
                        "%e \n\n", maxreldiff);
            }
            return std::make_pair(fail, total);
        }
};

template <unsigned N>
static std::pair<unsigned, unsigned> test(unsigned n_toys, unsigned the_seed)
{
    unsigned fail = 0, total = 0;
    TestCholesky<N> test(the_seed);
    for (unsigned i = 0; i < n_toys; ++i) {
        auto rc = test.run();
        fail += rc.first, total += rc.second;
    }
    std::printf("Summary %ux%u: %u out of %u failed\n", N, N,
            fail, total);
    std::printf("A %ux%u decomposition took %g ns/call.\n", N, N, test.time());
    return std::make_pair(fail, total);
}

int main(int argc, char* const argv[])
{
    using std::cout;
    using std::endl;
    cout << "--------------------------------------" << endl;
    cout << " trivial script to test Cholesky decomposition" << endl;
    cout << " Looking for" << endl;
    cout << " [1] Number of toys to run (default 1000)" << endl;
    cout << " [2] Seed for random numbers (default -1)" << endl;
    cout << "--------------------------------------" << endl;
    unsigned n_toys = 1000;
    unsigned the_seed = 0;
    cout << "testing Cholesky decomposition" << endl;
    if (argc > 1) {
        n_toys = atoi(argv[1]);
        if (argc > 2) {
            the_seed = atoi(argv[2]);
        }
    }
    if (0 == the_seed) the_seed = (unsigned) time(nullptr);
    cout << "using ntoys=" << n_toys << ", seed=" << the_seed << endl;

    std::pair<unsigned, unsigned> rc;
    unsigned fail = 0, total = 0;
    rc = test< 1>(n_toys, the_seed++);
    fail += rc.first, total += rc.second;
    rc = test< 2>(n_toys, the_seed++);
    fail += rc.first, total += rc.second;
    rc = test< 3>(n_toys, the_seed++);
    fail += rc.first, total += rc.second;
    rc = test< 4>(n_toys, the_seed++);
    fail += rc.first, total += rc.second;
    rc = test< 5>(n_toys, the_seed++);
    fail += rc.first, total += rc.second;
    rc = test< 6>(n_toys, the_seed++);
    fail += rc.first, total += rc.second;
    rc = test< 7>(n_toys, the_seed++);
    fail += rc.first, total += rc.second;
    rc = test< 8>(n_toys, the_seed++);
    fail += rc.first, total += rc.second;
    rc = test< 9>(n_toys, the_seed++);
    fail += rc.first, total += rc.second;
    rc = test<10>(n_toys, the_seed++);
    fail += rc.first, total += rc.second;
    //rc = test<11>(n_toys, the_seed++);
    //fail += rc.first, total += rc.second;
    cout << endl << "Summary: " << fail << " out of " <<
        total << " tests failed" << endl;

    // accept 150 ppm in failed toys - there's always a few
    double frate = double(fail) / double(total);
    bool okay = frate < 1.5e-4;
    cout << "         Failure rate: " << (1e6 * frate) << " ppm" <<
        (okay ? " is below threshold, all okay" :
         " is above threshold, FAILURE!") << endl;

    return !okay;
}

// vim: sw=4:ft=cpp:tw=78:et