/** @file HyperSphericalCoordinates.h
 *
 * @brief transform from and to hyperspherical coordinates
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2016-02-26
 */

#ifndef HYPERSPHERICALCOORDINATES_H
#define HYPERSPHERICALCOORDINATES_H

#include <cmath>
#include <type_traits>

/** @brief convert given vector to hyperspherical coordinates
 *
 * @tparam N        dimensionality of problem
 * @tparam TO       destination type
 * @tparam FROM     source type
 *
 * Any type that supports indexing operations using operator[] is admissible
 * as source and/or destination type.
 *
 * @param[OUT] to   destination for hyperspherical coordinates
 * @param[IN]  from source vector to convert
 *
 * This routine returns the quantities @f$(r, \phi, \theta_1, \ldots,
 * \theta_{N-2})@f$ in to, calculated from the vector given in from. @f$r@f$
 * is the radius, and the angle @f$\phi@f$ is in the range @f$[-\pi,\pi[@f$,
 * whereas the angles @f$\theta_2, \ldots, \theta_{N}@f$ are in the range
 * @f$[0,\pi]@f$. Vectors of zero length (for which the angles are not
 * indeterminate) have their angular components set to zero. Source and
 * destination need not be different.
 *
 * The formulae are the familiar ones: slightly generalised from the three
 * dimensional case, these relations relating the radius and the various
 * angles to the coordinates of the vector:
 *
 * @f[ \begin{array}{c c c c c c c c}
 * x_0 & = & r & \cos(\phi) & \sin(\theta_2) & \sin(\theta_3) & \ldots & \sin(\theta_N) \\
 * x_1 & = & r & \sin(\phi) & \sin(\theta_2) & \sin(\theta_3) & \ldots & \sin(\theta_N) \\
 * x_2 & = & r &            & \cos(\theta_2) & \sin(\theta_3) & \ldots & \sin(\theta_N) \\
 * x_3 & = & r &            &                & \cos(\theta_3) & \ldots & \sin(\theta_N) \\
 * \vdots &   & \vdots  &   &                &                & \ddots & \vdots         \\
 * x_N & = & r &            &                &                &        & \cos(\theta_N)
 * \end{array}@f]
 *
 * The inverse relations are given by:
 *
 * @f[ \begin{array}{r c l}
 * r              & = & \sqrt{x_0^2 + x_1^2 + \ldots + x_N^2} \\
 * \tan(\phi)     & = & \frac{x_1}{x_0} \\
 * \cos(\theta_2) & = & \frac{x_2}{\sqrt{x_0^2 + x_1^2 + x_2^2 }} \\
 * \cos(\theta_3) & = & \frac{x_3}{\sqrt{x_0^2 + x_1^2 + x_2^2 + x_3^2 }} \\
 * \vdots         &   & \vdots \\
 * \cos(\theta_N) & = & \frac{x_N}{\sqrt{x_0^2 + x_1^2 + x_2^2 + x_3^2 + \ldots + x_N^2 }} \\
 * \end{array} @f]
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2016-02-26
 */
template <unsigned N, typename TO, typename FROM>
void toHyperSphericalCoordinates(TO& to, const FROM& from) noexcept
{
    static_assert(0 != N, "Have to have at least one dimension!");
    if (1 == N) {
        // just pass through the coordinate
        to[0] = from[0];
    } else {
        // do the maths for two or more dimensions
        typedef typename std::decay<decltype(to[0])>::type value_type;
        value_type r2 = from[0] * from [0] + from[1] * from[1];
        to[1] = (value_type(0) == r2) ? value_type(0) :
                std::atan2(from[1], from[0]); // phi is tricky: four quadrants
        for (unsigned i = 2; i < N; ++i) { // calculate theta_2 ... theta_N
            r2 += from[i] * from[i];
            to[i] = std::acos( // map r^2 = 0 to zero angle
                ((value_type(0) == r2) ? value_type(1) : from[i]) /
                ((value_type(0) == r2) ? value_type(1) : std::sqrt(r2)));
        }
        to[0] = std::sqrt(r2);
    }
}

/** @brief convert from hyperspherical coordinates to a vector
 *
 * @tparam N        dimensionality of problem
 * @tparam TO       destination type
 * @tparam FROM     source type
 *
 * Any type that supports indexing operations using operator[] is admissible
 * as source and/or destination type.
 *
 * @param[OUT] to   destination vector
 * @param[IN]  from source vector with hyperspherical coordinates
 *
 * This routine returns the quantities @f$(x_0, x_1, \ldots, x_{N})@f$ in to,
 * calculated from the hyperspherical coordinates in from. @f$r@f$
 * is the radius, and the angle @f$\phi@f$ is in the range @f$[-\pi,\pi[@f$,
 * whereas the angles @f$\theta_2, \ldots, \theta_{N}@f$ are in the range
 * @f$[0,\pi]@f$. Vectors of zero length (for which the angles are not
 * indeterminate) have their angular components set to zero. Source and
 * destination need not be different.
 *
 * The formulae are the familiar ones: slightly generalised from the three
 * dimensional case, these relations relating the radius and the various
 * angles to the coordinates of the vector:
 *
 * @f[ \begin{array}{c c c c c c c c}
 * x_0 & = & r & \cos(\phi) & \sin(\theta_2) & \sin(\theta_3) & \ldots & \sin(\theta_N) \\
 * x_1 & = & r & \sin(\phi) & \sin(\theta_2) & \sin(\theta_3) & \ldots & \sin(\theta_N) \\
 * x_2 & = & r &            & \cos(\theta_2) & \sin(\theta_3) & \ldots & \sin(\theta_N) \\
 * x_3 & = & r &            &                & \cos(\theta_3) & \ldots & \sin(\theta_N) \\
 * \vdots &   & \vdots  &   &                &                & \ddots & \vdots         \\
 * x_N & = & r &            &                &                &        & \cos(\theta_N)
 * \end{array}@f]
 *
 * The inverse relations are given by:
 *
 * @f[ \begin{array}{r c l}
 * r              & = & \sqrt{x_0^2 + x_1^2 + \ldots + x_N^2} \\
 * \tan(\phi)     & = & \frac{x_1}{x_0} \\
 * \cos(\theta_2) & = & \frac{x_2}{\sqrt{x_0^2 + x_1^2 + x_2^2 }} \\
 * \cos(\theta_3) & = & \frac{x_3}{\sqrt{x_0^2 + x_1^2 + x_2^2 + x_3^2 }} \\
 * \vdots         &   & \vdots \\
 * \cos(\theta_N) & = & \frac{x_N}{\sqrt{x_0^2 + x_1^2 + x_2^2 + x_3^2 + \ldots + x_N^2 }} \\
 * \end{array} @f]
 *
 * @author Manuel Schiller <Manuel.Schiller@cern.ch>
 * @date 2016-02-26
 */
template <unsigned N, typename TO, typename FROM>
void fromHyperSphericalCoordinates(TO& to, const FROM& from) noexcept
{
    static_assert(0 != N, "Have to have at least one dimension!");
    typedef typename std::decay<decltype(to[0])>::type value_type;
    if (1 == N) { // 1D is always a bit special
        to[0] = from[0];
    } else { // two and more dimensions
        value_type s = from[0]; // accumulates product of sines of angles
        for (unsigned i = N; --i > 1; ) { // 3 and more dimensions
            value_type theta = from[i];
            to[i] = s * std::cos(theta);
            s *= std::sin(theta);
        }
        const value_type phi = from[1];
        to[0] = s * std::cos(phi), to[1] = s * std::sin(phi);
    }
}

#endif // HYPERSPHERICALCOORDINATES_H

// vim: sw=4:tw=78:ft=cpp:et
